# vuejs-scrimba

Série de tutoriels interactifs sur Scrimba - VueJS

Regarder les vidéos et faire les exercices issues de ce tutoriel sur Scrimba : https://scrimba.com/learn/vuedocs

## Détail

-   Rendu déclaratif
    -   Vue JS : https://fr.vuejs.org/v2/guide/index.html#Rendu-declaratif
    -   Scrimba : https://scrimba.com/scrim/cQ3QVcr?pl=pXKqta
-   Conditions et boucles
    -   Vue JS : https://fr.vuejs.org/v2/guide/index.html#Conditions-et-boucles
    -   Scrimba : https://scrimba.com/p/pXKqta/cEQe4SJ
-   Gestion des entrées utilisateur
    -   Vue JS : https://fr.vuejs.org/v2/guide/index.html#Gestion-des-entrees-utilisateur
    -   Scrimba : https://scrimba.com/p/pXKqta/czPNaUr
    -   Exercice en plus : https://gitlab.com/webausore/vuejs/introduction/gestion-entrees-utilisateur
-   Découper en composants
    -   Vue JS : https://fr.vuejs.org/v2/guide/index.html#Decouper-en-composants
    -   Scrimba : https://scrimba.com/p/pXKqta/cEQVkA3 / https://scrimba.com/learn/vuedocs/vuejs-tutorial-create-components-with-props-cgnEeh4
    -   Exercice en plus : https://gitlab.com/webausore/vuejs/introduction/decouper-en-composants

Exercice final : https://gitlab.com/webausore/vuejs/introduction/exercice-final
